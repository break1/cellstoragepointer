#!/usr/bin/python3.7

import unittest

import sys
import os

sys.path.append( os.path.abspath(os.curdir)  )

from  Lib.Section_Cell_Entity.CellAddress import SCellAddress

class TestCellAddress( unittest.TestCase):
    def testCellAddress(self):
        sData = "Section_1|555"
        addr = SCellAddress.fromString( sData )
        self.assertEqual( addr.toString(), sData )
        self.assertEqual( addr.section, "Section_1" )
        self.assertEqual( addr.cell,    "555" )

        addr = SCellAddress.defNotValid()
        self.assertIsNone( addr.section )
        self.assertIsNone( addr.cell )
        
if __name__ == '__main__':
    unittest.main()
