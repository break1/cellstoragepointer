import weakref

from PyQt5.QtWidgets import QGraphicsItem, QGraphicsEllipseItem, QGraphicsLineItem
from PyQt5.QtGui import QPen, QBrush, QColor, QFont
from PyQt5.QtCore import Qt, QRectF, QPointF

from Lib.Common.Base_GItem import CBase_GItem
from Lib.Common.TreeNode import CTreeNode

class CHomeLimit_SGItem(CBase_GItem):
    _R = 60
    def __init__(self, ISM, netObj, parent ):
        super().__init__( ISM, netObj, parent )
        self.setZValue( 100 )
    
    def calcBBox(self):
        self.prepareGeometryChange()
        self.BBoxRect = QRectF( -1* self._R, -1 * self._R, self._R*2, self._R*2 )
        d = self.fBBoxD
        self.BBoxRect_Adj = self.BBoxRect.adjusted(-1*d, -1*d, d, d)

    def paint(self, painter, option, widget):
        lod = option.levelOfDetailFromTransform( painter.worldTransform() )
        font = QFont()

        selection_color = Qt.darkRed
        bgColor = QColor(100, 100, 250)

        if lod < 0.03:
            bgColor = selection_color if self.isSelected() else bgColor
            painter.fillRect ( self.BBoxRect, bgColor )
        else:
            pen = QPen()

            if self.isSelected():
                pen.setColor( selection_color )
            else:
                pen.setColor( Qt.black )

            pen.setWidth( 10 )

            painter.setPen( pen )
            painter.setBrush( QBrush( bgColor, Qt.SolidPattern ) )
            if lod > 0.1:
                painter.drawEllipse( QPointF(0, 0), self._R, self._R  )
            else:
                painter.drawRect( 0-self._R/2, 0-self._R/2, self._R, self._R )

            if lod > 0.2:
                font = QFont()
                painter.rotate(-self.rotation())
                font.setPointSize(24)
                painter.setFont( font )
                painter.drawText( self.boundingRect(), Qt.AlignCenter, self.netObj().name )

        ## BBox
        if self.ISM.bDrawBBox == True:
            pen = QPen( Qt.blue )
            pen.setWidth( 4 )
            painter.setBrush( QBrush() )
            painter.setPen(pen)
            painter.drawRect( self.boundingRect() )
