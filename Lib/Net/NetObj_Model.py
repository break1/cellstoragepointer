
import re

from PyQt5.QtCore import Qt, QAbstractItemModel, QModelIndex

from .NetObj import CNetObj, SNOP
from .Net_Events import ENet_Event as EV
from .NetObj_Manager import CNetObj_Manager
from .NetObj_Proxy import CNetObj_Proxy
from Lib.Common.Utils import time_func        
from .NetObj_Proxy import gProxys

class CNetObj_Model( QAbstractItemModel ):
    def __init__( self, parent ):
        super().__init__( parent=parent)
        self.__rootProxy = None

        CNetObj_Manager.addCallback( EV.ObjCreated,       self )
        CNetObj_Manager.addCallback( EV.ObjPrepareDelete, self )

    def __del__( self ):
        if self.__rootProxy:
            del gProxys[ self.__rootProxy.UID ]

    def setRootNetObj( self, rootNetObj ):
        self.__rootProxy = CNetObj_Proxy( rootNetObj )
        self.modelReset.emit()

    ########################################################################

    def ObjCreated( self, netCmd ):
        netObj = CNetObj_Manager.accessObj( netCmd.Obj_UID, genAssert=True )
        parentProxy = CNetObj_Proxy.proxy_from_NetObjID( netObj.parent.UID ) if netObj.parent else None

        if parentProxy and parentProxy.bChildExpanded:
            parentIDX = self.proxy_To_Index( parentProxy )

            proxy = CNetObj_Proxy( netObj )
            
            indexInParent = parentProxy.getChildProxyCount()

            self.beginInsertRows( parentIDX, indexInParent, indexInParent )
            parentProxy.appendChildProxy( proxy )
            self.endInsertRows()

        self.layoutChanged.emit()

    def ObjPrepareDelete( self, netCmd ):
        netObj = CNetObj_Manager.accessObj( netCmd.Obj_UID, genAssert=True )
        parentProxy = CNetObj_Proxy.proxy_from_NetObjID( netObj.parent.UID ) if netObj.parent else None

        if parentProxy and parentProxy.bChildExpanded:
            try:
                parentIDX = self.proxy_To_Index( parentProxy )
            except ValueError:
                parentIDX = None

            if parentIDX:
                proxy = CNetObj_Proxy.queryProxy_from_NetObj( netObj )
                
                indexInParent = parentProxy.getChildProxyIndex( proxy )

                self.beginRemoveRows( parentIDX, indexInParent, indexInParent )
                parentProxy.removeChildProxy( proxy )
                self.endRemoveRows()

        self.layoutChanged.emit()


    ########################################################################

    # def removeRows ( self, row, count, parent ):
    #     self.beginRemoveRows( objIDX.parent(), objIDX.row(), objIDX.row() )
    #     self.endRemoveRows()
    #     return True

    def index( self, row, column, parentIdx ):
        if not self.hasIndex( row, column, parentIdx ):
            return QModelIndex()

        parentProxy = self.getProxy_or_Root( parentIdx )
        childProxy  = parentProxy.getChildProxy( row )

        if childProxy:
            return self.createIndex( row, column, childProxy )
        else:
            return QModelIndex()

    def parent( self, index ):
        if not index.isValid: return QModelIndex()

        proxy = self.proxy_From_Index( index )
        if proxy is None: return QModelIndex()

        parentProxy = proxy.parentProxy()
        if parentProxy is None: return QModelIndex()

        return self.proxy_To_Index( parentProxy )

    def rowCount( self, parentIndex ):
        if parentIndex.column() > 0: return 0

        proxy = self.getProxy_or_Root( parentIndex )

        if proxy:
            return proxy.getChildProxyCount()
        else:
            return 0

    def columnCount( self, parentIndex ): return CNetObj.modelDataColCount()

    ########################################################################
    
    def netObj_From_Index( self, index ):
        return self.proxy_From_Index( index ).netObj()

    def proxy_From_Index( self, index ):
        return index.internalPointer()
    
    def proxy_To_Index( self, proxy ):
        if proxy is self.__rootProxy: return QModelIndex()
        if proxy is None: return QModelIndex()
        
        parentProxy = proxy.parentProxy()
        if parentProxy is None: return QModelIndex()
        
        indexInParent = parentProxy.getChildProxyIndex( proxy )

        return self.createIndex( indexInParent, 0, proxy )

    def getProxy_or_Root( self, index ):
        if index.isValid():
            return index.internalPointer()
        else:
            return self.__rootProxy

    def indexes_By_UID( self, objSet ):
        indexes = set()
        for UID in objSet:
            netObj = CNetObj_Manager.accessObj( UID )
            indexes.add( self.proxy_To_Index( CNetObj_Proxy.queryProxy_from_NetObj( netObj ) ) )
        return indexes
    
    ########################################################################

    def headerData( self, section, orientation, role ):
        if( orientation != Qt.Horizontal ):
            return None

        if role == Qt.DisplayRole:
            return CNetObj.modelHeaderData( section )

    def data( self, index, role ):
        
        if not index.isValid(): return None

        proxy = self.proxy_From_Index( index )

        if role == Qt.DisplayRole or role == Qt.EditRole:
            if proxy:
                if proxy.netObj():
                    return proxy.netObj().modelData( index.column() )
                else:
                    return None
                    
    def setData( self, index, value, role = Qt.EditRole ):
        if not index.isValid(): return False
        if role != Qt.EditRole: return False

        proxy = self.proxy_From_Index( index )

        if proxy and proxy.netObj():
            proxy.netObj().rename( value )
        return True

    def flags( self, index ):
        flags = Qt.ItemIsSelectable | Qt.ItemIsEnabled

        proxy = self.proxy_From_Index( index )
        if proxy and proxy.netObj():
            if proxy.netObj().modelHeaderData( index.column() ) == SNOP.name:
              flags = flags | Qt.ItemIsEditable

        return flags

# QModelIndex CModelObjData::findByPath(const QString &sPath)
# {
# 	CModelObjDataProxy * pProxy = &m_Root;

# 	QStringList sl = sPath.split("/");

# 	for ( int i=1; i<sl.count(); ++i )
# 	{
# 		//pProxy->checkChildren(); // не нужно - вызовется в pProxy->child(

# 		CModelObjDataProxy * pChild = 0;
# 		for ( int j=0; j < pProxy->childrenCount(); ++j )
# 		{
# 			pChild = pProxy->child( j );
# 			if ( pChild->name() == sl.at(i) )
# 			{
# 				pProxy = pChild;
# 				break;
# 			}
# 		}
# 		if ( pProxy != pChild )
# 			return QModelIndex();
# 	}
# 	return getIndex( pProxy );
# }

    def getNextIndexForFind( self, index ):
        proxy = self.proxy_From_Index( index )
        if proxy.getChildProxyCount():            
            return self.proxy_To_Index( proxy.getChildProxy( 0 ) )

        sibling = index.sibling( (index.row() + 1 ), 0 )
        if( sibling.isValid() ): return sibling

        parent = index.parent()
        while( parent.isValid() ):
            parentSibling = parent.sibling( (parent.row() + 1 ), 0 )
            if( parentSibling.isValid() ): return parentSibling
            parent = parent.parent()

        return QModelIndex()

    def getPrevIndexForFind( self, index ):
        if index.row() == 0:
            return index.parent()

        proxy = self.proxy_From_Index( index.sibling( (index.row() - 1 ), 0 ) )

        while proxy.getChildProxyCount():
            proxy = proxy.getChildProxy( proxy.getChildProxyCount() - 1 )

        return self.proxy_To_Index( proxy )

    def findByName( self, sName, start_index, bNextOrPrev ):
        if not start_index.isValid():
            start_index = self.index( 0, 0, QModelIndex() )

        index = start_index
        while index.isValid():
            index = self.getNextIndexForFind( index ) if bNextOrPrev else self.getPrevIndexForFind( index )

            if index.isValid() and re.search( sName, index.data(), re.IGNORECASE):
                return index

        return QModelIndex()
