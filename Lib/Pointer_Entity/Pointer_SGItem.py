import weakref

from PyQt5.QtWidgets import QGraphicsItem, QGraphicsEllipseItem, QGraphicsLineItem
from PyQt5.QtGui import QPen, QBrush, QColor, QFont
from PyQt5.QtCore import Qt, QRectF, QPointF

from Lib.Common.Base_GItem import CBase_GItem
from Lib.Common.TreeNode import CTreeNode
from Lib.Section_Cell_Entity.Section_NetObject import sectionsNodeCache
from Lib.Section_Cell_Entity.Cell_NetObject import CCell_NO
from Lib.Section_Cell_Entity.Cell_SGItem import CCell_SGItem

class CPointer_SGItem(CBase_GItem):
    _R = 60
    def __init__(self, ISM, netObj, parent ):
        super().__init__( ISM, netObj, parent )
        self.setZValue( 50 )
        self.targetLine_SGItem = None
        self.targetSGItem = None
    
    def init(self):
        self.bFirstShow = False

        pen = QPen()
        pen.setWidth( 4 )
        pen.setColor( Qt.red )

        if self.targetLine_SGItem is None:
            self.targetLine_SGItem = QGraphicsLineItem( parent = self.ISM.SceneRoot_ParentGItem )
            self.targetLine_SGItem.setPen( pen )
            self.targetLine_SGItem.setZValue( 35 )

        if self.targetSGItem is None:
            self.targetSGItem = QGraphicsEllipseItem( parent = self.ISM.SceneRoot_ParentGItem )
            r = 40
            self.targetSGItem.setRect( -r, -r, r*2, r*2 )
            self.targetSGItem.setPen( pen )
            self.targetSGItem.setBrush( QBrush( Qt.NoBrush ) )
            self.targetSGItem.setZValue( 50 )

        super().init()
        
    def done(self):
        super().done()
        self.scene().removeItem( self.targetLine_SGItem )
        self.scene().removeItem( self.targetSGItem )

    def updatePos(self):
        super().updatePos()
        self.updateTargetPos()

    def parkTargetPos(self):
        x = self.netObj().x
        y = self.netObj().y
        if self.targetSGItem:
            self.targetSGItem.setPos( x, y )
        if self.targetLine_SGItem:
            self.targetLine_SGItem.setLine( x, y, x, y )

    def updateTargetPos(self):
        currCellAddress = self.netObj().getCurrentCell()

        if not currCellAddress.isValid():
            self.parkTargetPos()
            return
        
        section = currCellAddress.section
        cell    = currCellAddress.cell
        cell_NO = CTreeNode.resolvePath( sectionsNodeCache(), section + "/" + cell )
        if cell_NO is None:
            self.parkTargetPos()
            return

        assert isinstance( cell_NO, CCell_NO)
        cellGItem = self.ISM.GItems.get( cell_NO.UID )
        assert isinstance( cellGItem, CCell_SGItem )

        self.targetSGItem.setPos( cellGItem.cellCenter() )

        self.targetLine_SGItem.setLine( self.netObj().x, self.netObj().y, cellGItem.cellCenter().x(), cellGItem.cellCenter().y() )

    def calcBBox(self):
        self.prepareGeometryChange()
        self.BBoxRect = QRectF( -1* self._R, -1 * self._R, self._R*2, self._R*2 )
        d = self.fBBoxD
        self.BBoxRect_Adj = self.BBoxRect.adjusted(-1*d, -1*d, d, d)

    def paint(self, painter, option, widget):
        lod = option.levelOfDetailFromTransform( painter.worldTransform() )
        font = QFont()

        selection_color = Qt.darkRed
        bgColor = QColor(150, 200, 200)

        if lod < 0.03:
            bgColor = selection_color if self.isSelected() else bgColor
            painter.fillRect ( self.BBoxRect, bgColor )
        else:
            pen = QPen()

            if self.isSelected():
                pen.setColor( selection_color )
            else:
                pen.setColor( Qt.black )

            pen.setWidth( 10 )

            painter.setPen( pen )
            painter.setBrush( QBrush( bgColor, Qt.SolidPattern ) )
            if lod > 0.1:
                painter.drawEllipse( QPointF(0, 0), self._R, self._R  )
            else:
                painter.drawRect( 0-self._R/2, 0-self._R/2, self._R, self._R )

            if lod > 0.2:
                font = QFont()
                painter.rotate(-self.rotation())
                font.setPointSize(12)
                painter.setFont( font )
                painter.drawText( self.boundingRect(), Qt.AlignCenter, self.netObj().name )

        if not self.bFirstShow:
            self.bFirstShow = True
            self.updateTargetPos() ## may not good place in paint

        ## BBox
        if self.ISM.bDrawBBox == True:
            pen = QPen( Qt.blue )
            pen.setWidth( 4 )
            painter.setBrush( QBrush() )
            painter.setPen(pen)
            painter.drawRect( self.boundingRect() )
