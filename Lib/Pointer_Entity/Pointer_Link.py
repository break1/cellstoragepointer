
import time
import random

from Lib.Common.TickManager import CTickManager

from Lib.Net.NetObj_Utils import isSelfEvent
from Lib.Net.NetObj_Manager import CNetObj_Manager
from Lib.Net.Net_Events import ENet_Event as EV
from Lib.Pointer_Entity.Pointer_NetObject import SPP
from Lib.Section_Cell_Entity.CellAddress import SCellAddress, CCellAddressList
from Lib.Section_Cell_Entity.Section_NetObject import sectionsNodeCache

# Section_1|4,Section_2|22,Section_2|21,Section_1|1

class CPointer_Link():
    oneCellDelay = 2
    def __init__(self, netObj ):
        CTickManager.addTicker( 500, self.onTick )
        CNetObj_Manager.addCallback( EV.ObjPropUpdated, self )
        self.t = time.time()

    def ObjPropUpdated( self, netCmd ):
        if not isSelfEvent( netCmd, self.netObj() ): return

        if netCmd.sPropName in SPP.taskProps:
            self.t = time.time()

    def onTick(self):
        if self.netObj().test_mode:
            if self.netObj().task_list.count() == 0:
                self.getRandomTask()

        self.processTask()

    def getRandomTask( self ):
        if not sectionsNodeCache().childCount():
            return

        targetCount = random.randint(3, 10)

        targetList = []
        while len(targetList) < targetCount:
            randomSecNum = random.randint( 0, sectionsNodeCache().childCount()-1 )
            secList = list( sectionsNodeCache().children )
            sec = secList[ randomSecNum ]

            if not sec.childCount():
                continue

            randomCell = random.randint( 0, sec.childCount()-1 )
            cellList = list( sec.children )
            cell = cellList[ randomCell ]

            cellAddr = SCellAddress( section = sec.name, cell = cell.name )
            if cellAddr not in targetList:
                targetList.append( cellAddr )

        self.netObj().task_list = CCellAddressList( elementList = targetList )

    def processTask( self ):
        cell = self.netObj().getCurrentCell()
        if not cell.isValid(): return

        if time.time() - self.t < self.oneCellDelay: return

        if self.netObj().task_idx < self.netObj().task_list.count()-1:
            self.netObj().task_idx += 1
        else:
            self.netObj().task_list = CCellAddressList()
