import os
import json

from Lib.Net.NetObj import CNetObj
from Lib.Net.NetObj_Manager import CNetObj_Manager
import Lib.Net.NetObj_JSON as nJSON
from Lib.Net.NetObj_Utils import isSelfEvent
from Lib.Net.Net_Events import ENet_Event as EV
from Lib.Common.TreeNodeCache import CTreeNodeCache
from Lib.Common.StrConsts import SC
from Lib.Common.GeomAttrs import SGA
from Lib.Common.StrProps_Meta import СStrProps_Meta
from Lib.Section_Cell_Entity.CellAddress import CCellAddressList, SCellAddress

class SPointerProps( metaclass = СStrProps_Meta ):
    task_list = None
    task_idx  = None
    test_mode = None
SPP = SPointerProps
SPP.taskProps = [ SPointerProps.task_list, SPointerProps.task_idx ] # type:ignore

####################

class CPointer_NO( CNetObj ):
    def_props = { SGA.x: 0, SGA.y: 0, SGA.z: 0,
                  SPP.task_list: CCellAddressList(),
                  SPP.task_idx : 0,
                  SPP.test_mode : False }

    def __init__( self, name="", parent=None, id=None, saveToRedis=True, props=None, ext_fields=None ):
        super().__init__( name=name, parent=parent, id=id, saveToRedis=saveToRedis, props=props, ext_fields=ext_fields )
        CNetObj_Manager.addCallback( EV.ObjPropUpdated, self )

    def ObjPropUpdated( self, netCmd ):
        if not isSelfEvent( netCmd, self ): return

        if netCmd.sPropName == SPP.task_list:
            self.task_idx = 0

    def isTaskList_Valid(self):
        return ( self.task_list.count() != 0 ) and ( self.task_idx >= 0 ) and ( self.task_idx < self.task_list.count() )

    def getCurrentCell( self ):
        if not self.isTaskList_Valid():
            return SCellAddress.defNotValid()

        return self.task_list[ self.task_idx ]
