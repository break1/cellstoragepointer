import ctypes

import Lib.Common.FileUtils as FU

class esc_net_connection_t(ctypes.Structure):
    _fields_ = [('socket', ctypes.c_int)]

class esc_dev_t(ctypes.Structure):
    _fields_ = [('conn', esc_net_connection_t)]

class status_struct(ctypes.Structure):
    _fields_ = [('cmd', ctypes.c_int32 ),
                ('arg', ctypes.c_int32 )]

class esc_dev_status_t(ctypes.Structure):
    _fields_ = [('sn', ctypes.c_int32 ),
                ('status', status_struct)]

escdll = ctypes.CDLL( FU.projectDir() +  'Lib/esclib/libesc.so')

escdll.esc_connect.restype  = esc_dev_t
escdll.esc_connect.argtypes = [ ctypes.POINTER(ctypes.c_char), ctypes.c_int16 ]

escdll.esc_rotate.restype = ctypes.c_int
escdll.esc_rotate.argtypes = [ esc_dev_t, ctypes.POINTER( ctypes.c_float ), ctypes.POINTER( ctypes.c_float ) ]

escdll.esc_status.restype = ctypes.c_int
escdll.esc_status.argtypes = [ esc_dev_t, ctypes.POINTER( esc_dev_status_t ) ]

escdll.esc_disconnect.restype = ctypes.c_int
escdll.esc_disconnect.argtypes = [ esc_dev_t ]
