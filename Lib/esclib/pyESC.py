import ctypes

from enum import auto

from Lib.Common.BaseEnum import BaseEnum
from Lib.esclib.escdll import escdll, esc_dev_status_t

class EDevStatus( BaseEnum ):
    Ok           = 0
    MemError     = auto()
    NetError     = auto()
    ProtoError   = auto()
    CmdError     = auto()
    UnknownError = auto()

    Undefined = auto()
    Default   = Ok

class CEscDev:
    def __init__( self, sIpAddress, nPortNum ):
        self.__pDev = escdll.esc_connect( sIpAddress.encode(), nPortNum )

    def __del__( self ):
        status = escdll.esc_disconnect( self.__pDev )
        self.__checkError( status )
    
    def __checkError( self, c_status ):
        status = EDevStatus( c_status )
        if status != EDevStatus.Ok:
            print( f"Error work with board: { status }!" )

    def status( self ):
        status_struct = esc_dev_status_t()
        c_status = escdll.esc_status( self.__pDev, status_struct )
        return EDevStatus( c_status )

    def rotate( self, fA, fB ):
        status = escdll.esc_rotate( self.__pDev, ctypes.byref( ctypes.c_float( fA ) ), ctypes.byref( ctypes.c_float( fB ) ) )
        self.__checkError( status )
