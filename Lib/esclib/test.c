/*!
 *
 * \file      main.c
 *
 * \brief     Test programm
 *
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "esc.h"

void dev_reg(struct esc_dev_t *dev, char* ip, void *arg, uint32_t arglen)
{
    // INFO("Finded: %s", ip);
    float corner_u;
    float corner_v;

    // Rotate on 270 grd
    corner_u = 270.0;
    corner_v = 270.0;
    esc_rotate(dev,  &corner_u,  &corner_v);

    // Device update our varible with real corners
    // Out result of rotation
    printf("Rotated on %f:%f.\n", corner_u, corner_v);

    // Rotate backward on -270 grd
    corner_u = -270.0;
    corner_v = -270.0;
    esc_rotate(dev,  &corner_u,  &corner_v);

    // Device update our varible with real corners
    // Out result of rotation
    printf("Rotated on %f:%f.\n", corner_u, corner_v);

    // Rotate only in u-surface on 270grd
    corner_u = 270.0;
    corner_v = 0.0;
    esc_rotate(dev,  &corner_u,  &corner_v);

    // Device update our varible with real corners
    // Out result of rotation
    printf("Rotated on %f:%f.\n", corner_u, corner_v);

    // Rotate back only in u-surface on -270grd
    corner_u = -270.0;
    corner_v = 0.0;
    esc_rotate(dev,  &corner_u,  &corner_v);

    // Device update our varible with real corners
    // Out result of rotation
    printf("Rotated on %f:%f.\n", corner_u, corner_v);

    // Rotate only in v-surface on 270grd
    corner_u = 0.0;
    corner_v = 270.0;
    esc_rotate(dev,  &corner_u,  &corner_v);

    // Device update our varible with real corners
    // Out result of rotation
    printf("Rotated on %f:%f.\n", corner_u, corner_v);

    // Rotate back only in v-surface on -270grd
    corner_u = 0.0;
    corner_v = -270.0;
    esc_rotate(dev,  &corner_u,  &corner_v);

    // Device update our varible with real corners
    // Out result of rotation
    printf("Rotated on %f:%f.\n", corner_u, corner_v);

    // Disconnect
    esc_disconnect(dev);
}

int main(int argc, char *argv[])
{
    // Start search of device. When device finded call function dev_reg.
    // Stop find iteration when 1 device finded
    int ndev = esc_scan_net("10.150.125.10/30", 1010, &dev_reg, 1, NULL, 0);
//    int ndev = esc_scan_net("192.168.0.10/30", 1010, &dev_reg, 1, NULL, 0);


    // Print number of finded devices
    printf("Total: %d\n", ndev);

    return EXIT_SUCCESS;
}