#!/usr/bin/python3.7

import sys
import os
# требуется запуск из корня проекта
sys.path.append( os.path.abspath(os.curdir) )
sys.path.append( os.path.dirname( __file__ ) )

from Lib.esclib.escdll import escdll, esc_dev_status_t

import ctypes

pDev = escdll.esc_connect( "10.150.125.10".encode(), 1010 )
print( "connect", pDev, pDev.conn.socket )

st = escdll.esc_rotate( pDev, ctypes.byref( ctypes.c_float( 180.0 ) ), ctypes.byref( ctypes.c_float( -180.0 ) ) )
print( "rotate", st )

st = escdll.esc_rotate( pDev, ctypes.byref( ctypes.c_float( -360.0 ) ), ctypes.byref( ctypes.c_float( 360.0 ) ) )
print( "rotate", st )

status_struct = esc_dev_status_t()
st = escdll.esc_status( pDev, status_struct )
print( "status", st )

st = escdll.esc_disconnect( pDev )
print( "disconnect", st )
