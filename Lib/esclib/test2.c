#include "esc.h"
#include <stdio.h>

void checkStatus( struct esc_dev_t * pDev )
{
    struct esc_dev_status_t s;
    esc_api_err_t d = esc_status( pDev, &s );
    printf( " -------------- Device err status %d -------------- \n", d );
}

int main(int argc, char *argv[])
{
    printf( "11111111111111\n" );

    struct esc_dev_t * pDev;
    pDev = esc_connect( "10.150.125.10", 1010 );

    printf( "22222222222222\n" );
    checkStatus( pDev );

    float corner_u;
    float corner_v;

    corner_u = -360.0;
    corner_v = 360.0;
    esc_rotate(pDev,  &corner_u,  &corner_v);
    checkStatus( pDev );

    corner_u = 360.0;
    corner_v = -360.0;
    esc_rotate(pDev,  &corner_u,  &corner_v);
    checkStatus( pDev );

    printf( "333333333333333\n" );
    esc_disconnect(pDev);
    printf( "444444444444444\n" );
}