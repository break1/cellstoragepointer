/*!
 *
 * \file     esc.h
 *
 * \bief     Ethernet step motor controller interface description
 *
 */

#ifndef _ESC_H_
#define _ESC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#if defined(ENCRYPT) && (ENCRYPT==AES)
#include "aes.h"
#endif

/*!
 *
 * \typedef  esc_api_err_t
 *
 * \brief    API errors typedefs
 *
 * \param    ESC_API_NO_ERR       No erros in result
 * \param    ESC_API_MEM_ERR      Memory allocation error. Violation of access rights or boundaries when accessing memory.
 * \param    ESC_API_NET_ERR      Error communicating over the network.
 * \param    ESC_API_PROTO_ERR    Error in the interaction Protocol.
 * \param    ESC_API_CMD_ERR      Error executing a command on the device.
 * \param    ESC_API_UNK_ERR      An error not covered by the above.
 *
 */

typedef enum { ESC_API_NO_ERR, ESC_API_MEM_ERR, ESC_API_NET_ERR, ESC_API_PROTO_ERR, ESC_API_CMD_ERR, ESC_API_UNK_ERR} esc_api_err_t;

/*
 * \struct     esc_net_connection_t
 *
 * \brief      Device connection context
 *
 * \param      int            socket    Communication socket
 * \param      struct AES_ctx ctx       Encryption context
 *
 */
struct esc_net_connection_t
{
    int    socket;
    // Encryption support
#if defined(ENCRYPT) && (ENCRYPT==AES)
    struct AES_ctx ctx;
#endif
};

/*
 *
 * \struct    esc_dev_t
 *
 * \brief     Device context
 *
 * \param     struct esc_net_connection_t   conn    Device connection context
 *
 */
struct esc_dev_t
{
    struct esc_net_connection_t conn;
};

/*
 *
 * \struct    esc_dev_status_t
 *
 * \brief     Device status structure
 *
 * \param     status  { cmd, arg }    device status structure cmd is current execution command
 *                                                            arg current command argument
 * \param     sn                      serial number
 *
 */
struct esc_dev_status_t
{
    uint32_t                sn;
    struct
    {
        uint32_t cmd:8;
        uint32_t arg:24;
    }                       status;
};

/*
 * \typdef    esc_callback_t
 *
 * \brief     API callback function. Used for calling user function in some API functions.
 *                                   For example: esc_scan_net, esc_scan_net_list, esc_scan_list
 *
 * \param     struct esc_dev_t * dev        Pointer to device context structure
 * \param     char *             ip         Pointer to ip address string
 * \param     void *             arg        Pointer to user space object as calling argument
 * \param     uint32_t           argsize    Size in bytes of user space object
 *
 */
typedef void (*esc_callback_t)(struct esc_dev_t *, char *, void *, uint32_t);

/*!
 * \fn       struct esc_dev_t *esc_connect(char* ip, uint32_t port, uint8_t *key, uint8_t *iv)
 *
 * \brief    Devive context initialization
 *
 * \param    char     *ip    String valie of ip address,
 * \param    uint16_t  port  Device port
 *
 * \retval   Pointer to device context structure
 *
 */
struct esc_dev_t *esc_connect(char *, uint16_t);

/*!
 * \fn       esc_api_err_t esc_disconnect(struct esc_dev_t *dev)
 *
 * \brief    DISCONNECTION command
 *
 * \param    struct esc_dev_t *dev
 *
 * \retval   Command result
 *
 */
esc_api_err_t esc_disconnect(struct esc_dev_t *);

/*!
 * \fn       esc_api_err_t esc_status(struct esc_dev_t *dev, struct esc_dev_status_t *status)
 *
 * \brief    STATUS command
 *
 * \param    struct esc_dev_t        *dev       device context structure pointer
 * \param    struct esc_dev_status_t *status    device status structure pointer
 *
 * \retval   Command result
 *
 */
esc_api_err_t esc_status(struct esc_dev_t *, struct esc_dev_status_t *);

/*!
 * \fn       esc_api_err_t esc_calibrate(struct esc_dev_t *dev, float *u, float *v)
 *
 * \brief    Update command
 *
 * \param    struct esc_dev_t *dev
 * \param    float            *u    pointer to corner on surface u
 * \param    float            *v    pointer to corner on surface v
 *
 * \retval   Command          result
 * \retval   float            *u pointer to corner on u surface
 * \retval   float            *v pointer to corner on v surface
 *
 */
esc_api_err_t esc_calibrate(struct esc_dev_t *, float *, float *);

/*!
 * \fn       esc_api_err_t esc_reboot(struct esc_dev_t *dev)
 *
 * \brief    REBOOT command
 *
 * \param    struct esc_dev_t *dev
 *
 * \retval   Command result
 *
 */
esc_api_err_t esc_reboot(struct esc_dev_t *);

/*!
 * \fn       esc_api_err_t esc_rotate(struct esc_dev_t *dev, float *u, float *v)
 *
 * \brief    ROTATE command
 *
 * \param    struct esc_dev_t *dev
 * \param    float            *u pointer to corner on u surface
 * \param    float            *v pointer to corner on v surface
 *
 * \retval   Command          result
 * \retval   float            *u pointer to corner on u surface
 * \retval   float            *v pointer to corner on v surface
 *
 */
esc_api_err_t esc_rotate(struct esc_dev_t *, float *, float *);

/*!
 * \fn       esc_api_err_t esc_update(struct esc_dev_t *dev, char *filename)
 *
 * \brief    UPDATE command
 *
 * \param    struct esc_dev_t *dev
 *
 * \retval   Command result
 *
 * \note     !In the development! Not full functional.
 *
 */
esc_api_err_t esc_update(struct esc_dev_t *, char *);


/*!
 * \fn       int esc_scan_net(const char *strnet, uint16_t port, esc_callback_t func, uint8_t ndev)
 *
 * \brief    Device context initialization
 *
 * \param    char              *net    Network addfress with network prefix
 * \param    uint16_t           port   Device port for connection when check
 * \param    esc_callback_t     func   User callback function
 * \param    uint8_t            ndev   Number of dev when find end
 *
 * \retval   return number of finded devices
 *
 */
int esc_scan_net(const char *, uint16_t, esc_callback_t, uint8_t, void *, uint32_t);

/*!
 * \fn       int esc_scan_net_list(const char *list, uint16_t port, esc_callback_t func, uint8_t ndev)
 *
 * \brief    Devive context initialization
 *
 * \param    char              *list   LISt of comma delimited network addrress with network prefix
 * \param    uint16_t           port   Device port for connection when check
 * \param    esc_callback_t     func   User callback function
 * \param    uint8_t            ndev   Number of dev when find end
 *
 * \retval   return number of finded devices
 *
 */
int esc_scan_net_list(const char *, uint16_t, esc_callback_t, uint8_t, void *, uint32_t);

/*!
 * \fn       int esc_scan_list(const char *list, uint16_t port, esc_callback_t func, uint8_t ndev)
 *
 * \brief    Scan list and call user function
 *
 * \param    char              *list    Comma delimited list of ip address
 * \param    uint16_t           port    Device port for connection when check
 * \param    esc_callback_t     func    User callback function
 * \param    uint8_t            ndev    Number of dev when find end
 *
 * \retval   return number of finded devices
 *
 */
int esc_scan_list(const char *, uint16_t, esc_callback_t, uint8_t, void *, uint32_t);

#ifdef __cplusplus
}
#endif

#endif /* _ESC_H_ */
