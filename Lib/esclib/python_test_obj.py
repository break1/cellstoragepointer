#!/usr/bin/python3.7

import sys
import os
# требуется запуск из корня проекта
sys.path.append( os.path.abspath(os.curdir) )
sys.path.append( os.path.dirname( __file__ ) )

from pyESC import CEscDev

escObj = CEscDev( "10.150.125.10", 1010 )

escObj.rotate( 180.0, -180.0 )

print( escObj.status() )

escObj.rotate( -180.0, 180.0 )

print( escObj.status() )
