import re

from Lib.Common.StrConsts import genSplitPattern, SC
from Lib.Common.SerializedList import CSerializedList

class SCellAddress:
    DS = "|"
    DS_split_pattern = genSplitPattern( f"\{DS}" )

    def __init__( self, section, cell ):
        self.section = section
        self.cell = cell

    @classmethod
    def defNotValid( cls ):
        return SCellAddress( None, None )

    def isValid( self ):
        return ( self.section is not None ) and ( self.cell is not None )

    def __str__( self ): return self.toString()

    def __eq__( self, other ):
        eq = True
        eq = eq and self.section == other.section
        eq = eq and self.cell == other.cell
        return eq

    @classmethod
    def fromString( cls, data ):
        l = re.split( cls.DS_split_pattern, data )

        try:
            section = l[0]
            cell     = l[1]
        except:
            print( f"{SC.sError} SCellAddress can't convert from '{data}'!" )
            section = ""
            cell    = ""

        return SCellAddress( section, cell )

    def toString( self ): return f"{self.section}{ self.DS }{self.cell}"

############

class CCellAddressList( CSerializedList ):
    element_type = SCellAddress
