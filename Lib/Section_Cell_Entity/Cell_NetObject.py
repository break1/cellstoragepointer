import os
import json

from Lib.Net.NetObj import CNetObj
from Lib.Net.NetObj_Manager import CNetObj_Manager
import Lib.Net.NetObj_JSON as nJSON
from Lib.Net.NetObj_Utils import isSelfEvent
from Lib.Net.Net_Events import ENet_Event as EV
from Lib.Common.TreeNodeCache import CTreeNodeCache
from Lib.Common.StrConsts import SC
from Lib.Common.GeomAttrs import SGA

####################

class CCell_NO( CNetObj ):
    def_props = { SGA.x: 0, SGA.y: 0, SGA.w: 400, SGA.h: 300 }

    def cellCenter( self ):
        x = self.w / 2
        y = self.h / 2
        return x, y

    # def __init__( self, name="", parent=None, id=None, saveToRedis=True, props=None, ext_fields=None ):
    #     super().__init__( name=name, parent=parent, id=id, saveToRedis=saveToRedis, props=props, ext_fields=ext_fields )
