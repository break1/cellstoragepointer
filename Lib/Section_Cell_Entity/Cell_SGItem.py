import weakref

from PyQt5.QtWidgets import QGraphicsItem
from PyQt5.QtGui import QPen, QBrush, QColor, QFont
from PyQt5.QtCore import Qt, QRectF

from Lib.Common.Base_GItem import CBase_GItem

class CCell_SGItem(CBase_GItem):
    def __init__(self, ISM, netObj, parent ):
        super().__init__( ISM, netObj, parent )
        self.setZValue( 40 )

    def cellCenter( self ):
        x, y = self.netObj().cellCenter()
        return self.mapToScene( x, y )

    def paint(self, painter, option, widget):
        lod = option.levelOfDetailFromTransform( painter.worldTransform() )

        selection_color = Qt.darkRed
        bgColor = QColor(205, 145, 40)

        if lod < 0.03:
            bgColor = selection_color if self.isSelected() else bgColor
            painter.fillRect ( self.BBoxRect, bgColor )
        else:
            pen = QPen()

            if self.isSelected():
                pen.setColor( selection_color )
            else:
                pen.setColor( Qt.black )

            pen.setWidth( 10 )

            painter.setPen( pen )
            painter.setBrush( QBrush( bgColor, Qt.SolidPattern ) )
            painter.drawRect( self.BBoxRect )

            font = QFont()
            font.setPointSize(32)
            painter.setFont( font )
            painter.drawText( self.boundingRect(), Qt.AlignCenter, self.netObj().name )

        ## BBox
        if self.ISM.bDrawBBox == True:
            pen = QPen( Qt.blue )
            pen.setWidth( 4 )
            painter.setBrush( QBrush() )
            painter.setPen(pen)
            painter.drawRect( self.boundingRect() )
