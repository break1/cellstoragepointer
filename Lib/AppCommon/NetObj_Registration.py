
from distutils.util import strtobool

from Lib.Net.NetObj_Manager import CNetObj_Manager
from Lib.Net.NetObj import CNetObj
from Lib.Net.DictProps_Widget import CDictProps_Widget
from Lib.Net.NetObj_Widgets import CNetObj_WidgetsManager, CNetObj_Widget

from Lib.Common.StrConsts import SC
import Lib.Common.BaseTypes as BT
from Lib.Common.StrTypeConverter import CStrTypeConverter as STC
from Lib.Common.SerializedList import CStrList

from Lib.Section_Cell_Entity.Cell_NetObject import CCell_NO
from Lib.Section_Cell_Entity.Section_NetObject import CSection_NO
from Lib.Section_Cell_Entity.CellAddress import CCellAddressList
from Lib.Pointer_Entity.Pointer_NetObject import CPointer_NO
from Lib.Pointer_Entity.Pointer_Link import CPointer_Link
from Lib.HomeLimit_Entity.HomeLimit_NetObject import CHomeLimit_NO

rootObjDict = { SC.Pointers : CNetObj,
                SC.Sections : CNetObj,
                SC.Limits   : CNetObj }

def register_NetObj():
    reg = CNetObj_Manager.registerType
    reg( CNetObj )
    reg( CCell_NO )
    reg( CSection_NO )
    reg( CPointer_NO )
    reg( CHomeLimit_NO )

def register_NetObj_Props():
    STC.registerStdType( int )
    STC.registerStdType( str )
    STC.registerStdType( float )
    STC.registerType( bool, from_str_func=lambda val : bool(strtobool( val )) if type(val)==str else bool(val) )
    STC.registerUserType( CStrList )
    STC.registerUserType( BT.CConnectionAddress )
    STC.registerUserType( CCellAddressList )

def register_NetObj_Controllers():
    reg = CNetObj_Manager.registerController
    reg( CPointer_NO, CPointer_Link )
