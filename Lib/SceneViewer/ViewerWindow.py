
import sys
import os

from PyQt5.QtCore import pyqtSlot, QByteArray, Qt, pyqtSlot
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QPainterPath
from PyQt5.QtWidgets import QGraphicsView, QGraphicsScene, QMainWindow, QFileDialog, QMessageBox, QAction, QDockWidget, QLabel, QInputDialog, QActionGroup
from PyQt5 import uic

from Lib.Common.GridGraphicsScene import CGridGraphicsScene
from Lib.Common.GV_Wheel_Zoom_EventFilter import CGV_Wheel_Zoom_EF
from Lib.Common.SettingsManager import CSettingsManager as CSM
from Lib.Common.BaseApplication import EAppStartPhase
from Lib.Common.StrConsts import SC


import Lib.Common.FileUtils as FU
from Lib.Common.GuiUtils import gvFitToPage, load_Window_State_And_Geometry, save_Window_State_And_Geometry
from Lib.Common.Utils import time_func
from Lib.Common.StrProps_Meta import СStrProps_Meta
from Lib.Common.TickManager import CTickManager

from Lib.Net.NetObj_Widgets import CNetObj_WidgetsManager
from Lib.Net.NetObj import CNetObj
from Lib.Net.NetObj_Manager import CNetObj_Manager
from Lib.Net.NetObj_Monitor import CNetObj_Monitor
from Lib.Net.DictProps_Widget import CDictProps_Widget

from Lib.Section_Cell_Entity.Cell_NetObject import CCell_NO

from Lib.SceneViewer.Item_Scene_Manager import CItem_Scene_Manager, ESelectionMode, EAddObjMode
from .images_rc import *
from enum import Enum, auto

class SSceneOptions( metaclass = СStrProps_Meta ):
    scene              = None
    grid_size          = None
    draw_grid          = None
    snap_to_grid       = None
    draw_bbox          = None
    lock_editing       = None

SSO = SSceneOptions

###########################################
sceneDefSettings = {
                    SSO.grid_size           : 400,  
                    SSO.draw_grid           : False,
                    SSO.snap_to_grid        : False,
                    SSO.draw_bbox           : False,
                    SSO.lock_editing        : False
                    }
###########################################


class EWorkMode( Enum ):
    DesignerMode = auto()
    NetMonitorMode  = auto()

# Storage Map Designer / Storage Net Monitor  Main Window
class CViewerWindow(QMainWindow):
    __sWorkedArea = "Worked area: "

    def registerObjects_Widgets(self):
        reg = self.WidgetManager.registerWidget
        reg( CCell_NO,  CDictProps_Widget, "Props" )

    def __init__(self, windowTitle = "", workMode = EWorkMode.DesignerMode):
        super().__init__()

        self.workMode = workMode
        self.__sWindowTitle = windowTitle
        self.selectedGItem = None

        uic.loadUi( FU.UI_fileName( __file__ ), self )
        self.setWindowTitle( self.__sWindowTitle )

        CTickManager.addTicker( 500, self.tick )

        self.bFullScreen = False
        self.DocWidgetsHiddenStates = {}
        self.currentSceneFName = SC.defSceneFName

        self.GScene = CGridGraphicsScene( self )
        self.GScene.selectionChanged.connect( self.GScene_SelectionChanged )

        self.Scene_View.setScene( self.GScene )

        self.ISM = CItem_Scene_Manager( self.GScene, self.Scene_View )
        
        # hide some options when not in designer mode
        b = self.workMode == EWorkMode.DesignerMode
        self.acSnapToGrid.setVisible( b )
        self.acLockEditing.setVisible( b )
        self.acAlignHorisontal.setVisible( b )
        self.acAlignVertical.setVisible( b )
        self.acMoveX.setVisible( b )
        self.acMoveY.setVisible( b )
        self.toolEdit.setVisible( b )
        self.acNewScene.setVisible( b )
        self.acLoadScene.setVisible( b )
        self.acSaveScene.setVisible( b )
        self.acSaveSceneAs.setVisible( b )
        self.acAddSection.setVisible( b )
        self.acAddCell.setVisible( b )

        self.lbWorkedArea = QLabel()
        self.statusbar.addWidget( self.lbWorkedArea )

        self.GV_Wheel_Zoom_EF = CGV_Wheel_Zoom_EF(self.Scene_View)

        self.WidgetManager = CNetObj_WidgetsManager( self.dkObjectWdiget_Contents )
        self.registerObjects_Widgets()

    def init( self, initPhase ):            
        if initPhase == EAppStartPhase.BeforeRedisConnect:
            self.loadSettings()

        elif initPhase == EAppStartPhase.AfterRedisConnect:
            objMonitor = CNetObj_Monitor.instance
            if objMonitor:
                objMonitor.SelectionChanged_signal.connect( self.doSelectObjects )
            if self.workMode == EWorkMode.DesignerMode:
                sLastOpenedFName = CSM.rootOpt( SC.last_opened_file, default="" )
                if sLastOpenedFName:
                    self.load( path = sLastOpenedFName )
                
    #############################################################################
    def loadSettings( self ):
        load_Window_State_And_Geometry( self )

        sceneSettings = CSM.rootOpt( SSO.scene, default=sceneDefSettings )

        self.GScene.gridSize     = CSM.dictOpt( sceneSettings, SSO.grid_size,     default = self.GScene.gridSize )
        self.GScene.bDrawGrid    = CSM.dictOpt( sceneSettings, SSO.draw_grid,     default = self.GScene.bDrawGrid )
        self.GScene.bSnapToGrid  = CSM.dictOpt( sceneSettings, SSO.snap_to_grid,  default = self.GScene.bSnapToGrid)
        self.ISM.setDrawBBox         ( CSM.dictOpt( sceneSettings, SSO.draw_bbox, default = self.ISM.bDrawBBox ) )
        self.ISM.bReadOnly       = CSM.dictOpt( sceneSettings, SSO.lock_editing,  default = self.workMode != EWorkMode.DesignerMode )
        if self.workMode != EWorkMode.DesignerMode:
            self.ISM.bReadOnly = True

        #setup ui
        self.sbGridSize.setValue     ( self.GScene.gridSize )
        self.acGrid.setChecked       ( self.GScene.bDrawGrid )
        self.acBBox.setChecked       ( self.ISM.bDrawBBox )
        self.acSnapToGrid.setChecked ( self.GScene.bSnapToGrid )
        self.acLockEditing.setChecked( self.ISM.bReadOnly )

    def saveSettings( self ):
        save_Window_State_And_Geometry( self )

        CSM.options[ SSO.scene ] =   {
                                        SSO.grid_size           : self.GScene.gridSize,
                                        SSO.draw_grid           : self.GScene.bDrawGrid,
                                        SSO.snap_to_grid        : self.GScene.bSnapToGrid,
                                        SSO.draw_bbox           : self.ISM.bDrawBBox,
                                        SSO.lock_editing        : self.ISM.bReadOnly
                                     }
    #############################################################################

    def update_ViewPortArea_Info(self):
        rectf = self.Scene_View.mapToScene(self.Scene_View.viewport().geometry()).boundingRect()
        self.lbWorkedArea.setText( f"{self.__sWorkedArea} X={round(rectf.left())} Y={round(rectf.top())} W={round(rectf.width())} H={round(rectf.height())}" )

    def update_WindowTitle(self):
        #добавляем '*' в заголовок окна если есть изменения
        sign = "" if not self.ISM.bHasChanges else "*"
        self.setWindowTitle( f"{self.__sWindowTitle} {self.currentSceneFName}{sign}" )

    ###########################################

    def unhideDocWidgets(self):
        for doc in self.DocWidgetsHiddenStates:
            isHidden = self.DocWidgetsHiddenStates[doc]
            if not isHidden: doc.show()

    def hideDocWidgets(self):
        DocWidgetsList = [ doc for doc in self.children() if isinstance( doc, QDockWidget ) ]
        for doc in DocWidgetsList:
            self.DocWidgetsHiddenStates[ doc ] = doc.isHidden()
            doc.hide()

    def tick(self):
        #форма курсора
        self.updateCursor()

        self.update_ViewPortArea_Info()

        if self.workMode != EWorkMode.DesignerMode: return

        self.update_WindowTitle()
        b = not self.ISM.bReadOnly
        self.acAlignHorisontal.setEnabled( b )
        self.acAlignVertical.setEnabled( b )
        self.acMoveX.setEnabled( b )
        self.acMoveY.setEnabled( b )
        self.acAddSection.setEnabled( b )
        self.acAddCell.setEnabled( b )


    def updateCursor(self):
        pass
        # if self.GV_Wheel_Zoom_EF.actionCursor != Qt.ArrowCursor:
        #     self.Scene_View.setCursor( self.GV_Wheel_Zoom_EF.actionCursor )
        # elif self.ISM.EditMode & EGManagerEditMode.AddNode:
        #     self.Scene_View.setCursor( Qt.CrossCursor )
        # else:
        #     if self.ISM.selectionMode == ESelectionMode.Touch:
        #         self.Scene_View.setCursor( Qt.CrossCursor )
        #     else:
        #         self.Scene_View.setCursor( Qt.ArrowCursor )

    def closeEvent( self, event ):
        if self.workMode == EWorkMode.DesignerMode:
            if not self.unsavedChangesDialog():
                event.ignore()
                return

        self.saveSettings()

    def unsavedChangesDialog(self):
        if self.ISM.bHasChanges:
            mb =  QMessageBox(0,'', "Save changes to document before closing?", QMessageBox.Cancel | QMessageBox.Save)
            mb.addButton("Close without saving", QMessageBox.RejectRole )
            res = mb.exec()
        
            if res == QMessageBox.Save:
                self.on_acSaveScene_triggered(True)

            elif res == QMessageBox.Cancel:
                return False
        
        return True

    # событие изменения выделения на сцене
    def GScene_SelectionChanged( self ):
        s = set()
        for gItem in self.GScene.selectedItems():
            s = s.union( gItem.getNetObj_UIDs() )
        
        objMonitor = CNetObj_Monitor.instance
        if objMonitor:
            objMonitor.doSelectObjects( s )
        else:
            self.updateObjWidget( s )

    def updateObjWidget( self, objUID_Set ):
        l = list( objUID_Set )
        if len( l ) == 1:
            netObj = CNetObj_Manager.accessObj( l[0] )
            self.WidgetManager.activateWidgets( netObj )
        else:
            self.WidgetManager.clearActiveWidgets()

    # слот для реакции на выделение объектов в мониторе объектов
    @pyqtSlot( set )
    def doSelectObjects( self, objSet ):
        self.ISM.selectItemsByUID( objSet )
        self.updateObjWidget( objSet )
                
    @pyqtSlot("bool")
    def on_acFitToPage_triggered(self, bChecked):
        gvFitToPage( self.Scene_View )

    @pyqtSlot(bool)
    def on_acZoomIn_triggered(self, bChecked):
        self.GV_Wheel_Zoom_EF.zoomIn()

    @pyqtSlot(bool)
    def on_acZoomOut_triggered(self, bChecked):
        self.GV_Wheel_Zoom_EF.zoomOut()

    @pyqtSlot()
    def on_acFullScreen_triggered(self):
        self.bFullScreen = not self.bFullScreen

        if self.bFullScreen:
            self.hideDocWidgets()
            self.geometry = self.saveGeometry()
            self.showMaximized()
        else:
            self.unhideDocWidgets()
            self.restoreGeometry(self.geometry)

    @pyqtSlot(bool)
    def on_acGrid_triggered(self, bChecked):
        self.GScene.setDrawGrid( bChecked )

    @pyqtSlot(bool)
    def on_acSnapToGrid_triggered(self, bChecked):
        self.GScene.bSnapToGrid = bChecked

    @pyqtSlot(bool)
    def on_acBBox_triggered(self, bChecked):
        self.ISM.setDrawBBox(bChecked)

    @pyqtSlot(bool)
    def on_acSpecialLines_triggered(self, bChecked):
        self.ISM.setDrawSpecialLines( bChecked )

    @pyqtSlot(bool)
    def on_acLockEditing_triggered( self, bChecked ):
        self.ISM.bReadOnly = bChecked

    @pyqtSlot()
    def on_acSelectAll_triggered(self):
        selectionPath = QPainterPath()
        selectionPath.addRect( self.GScene.itemsBoundingRect() )
        self.GScene.setSelectionArea( selectionPath )

    @pyqtSlot()
    def on_acAlignVertical_triggered(self):
        self.ISM.alignGItemsVertical()
    
    @pyqtSlot()
    def on_acAlignHorisontal_triggered(self):
        self.ISM.alignGItemsHorisontal()

    @pyqtSlot(bool)
    def on_acMoveX_triggered(self):
        delta_X, ok = QInputDialog.getInt( self, "Set delta X", "" )
        if ok:
            self.ISM.moveGItems( GItems = self.GScene.selectedItems(), x = delta_X, y = 0 )

    @pyqtSlot(bool)
    def on_acMoveY_triggered(self):
        delta_Y, ok = QInputDialog.getInt( self, "Set delta Y", "" )
        if ok:
            self.ISM.moveGItems( GItems = self.GScene.selectedItems(), x = 0, y = delta_Y )

    @pyqtSlot()
    def on_sbGridSize_editingFinished(self):
        self.GScene.setGridSize( self.sbGridSize.value() )

    ###########################################
    def load( self, path = None ):
        def beforeLoad():
            CNetObj_Manager.rootObj.destroyChildren()
            self.ISM.clear()
            self.ISM.init()
        path = CNetObj_Monitor.loadObj( parentW=self, parent=CNetObj_Manager.rootObj, bIgnoreDuplicates=True, onLoad = beforeLoad, path=path )
        if path:
            self.currentSceneFName = path
            CSM.options[ SC.last_opened_file ] = self.currentSceneFName
            self.ISM.bHasChanges = False
            self.on_acFitToPage_triggered( True )


    @pyqtSlot(bool)
    def on_acNewScene_triggered(self, bChecked):
        CNetObj_Manager.rootObj.destroyChildren()
        self.ISM.clear()
        self.ISM.init()
        self.currentSceneFName = SC.defSceneFName
        CSM.options[ SC.last_opened_file ] = ""
        self.app.initBaseRootObj()

    @pyqtSlot(bool)
    def on_acLoadScene_triggered(self, bChecked):
        self.load()

    @pyqtSlot(bool)
    def on_acSaveScene_triggered(self, bChecked):
        if self.currentSceneFName == SC.defSceneFName:
            self.on_acSaveSceneAs_triggered(True)
        else:
            CNetObj_Monitor.saveObj( parentW = self, netObj=CNetObj_Manager.rootObj, bOnlyChild=True, path = self.currentSceneFName )
            self.ISM.bHasChanges = False

    @pyqtSlot(bool)
    def on_acSaveSceneAs_triggered(self, bChecked):
        path = CNetObj_Monitor.saveObj( parentW = self, netObj=CNetObj_Manager.rootObj, bOnlyChild=True )
        if path:
            self.currentSceneFName = path
            CSM.options[ SC.last_opened_file ] = self.currentSceneFName
            self.ISM.bHasChanges = False

    @pyqtSlot(bool)
    def on_acAddCell_triggered(self, bChecked):
        if not bChecked:
            self.ISM.addObjMode = EAddObjMode.NoObj
            return

        self.acAddSection.setChecked( False )
        self.ISM.addObjMode = EAddObjMode.Cell

    @pyqtSlot(bool)
    def on_acAddSection_triggered(self, bChecked):
        if not bChecked:
            self.ISM.addObjMode = EAddObjMode.NoObj
            return
        
        self.acAddCell.setChecked( False )
        self.ISM.addObjMode = EAddObjMode.Section
