
import os
import weakref
import math
import copy
from enum import Flag, auto, Enum
from copy import deepcopy
from collections import namedtuple

from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import pyqtSlot, QObject, QLineF, QPointF, QEvent, Qt, pyqtSignal, QTimer
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsLineItem, QGraphicsScene

from Lib.Net.NetObj_Manager import CNetObj_Manager
from Lib.Net.NetObj import CNetObj

from Lib.Common.GuiUtils import gvFitToPage
from Lib.Common.Utils import time_func
from Lib.Common.TreeNodeCache import CTreeNodeCache
from Lib.Common.StrConsts import SC
from Lib.Common.Dummy_GItem import CDummy_GItem
from Lib.Common.Vectors import Vector2
from Lib.Common.GeomAttrs import SGA

from Lib.Net.Net_Events import ENet_Event as EV

from Lib.Section_Cell_Entity.Cell_NetObject import CCell_NO
from Lib.Section_Cell_Entity.Cell_SGItem import CCell_SGItem
from Lib.Section_Cell_Entity.Section_NetObject import CSection_NO, sectionsNodeCache
from Lib.Section_Cell_Entity.Section_SGItem import CSection_SGItem

from Lib.Pointer_Entity.Pointer_NetObject import CPointer_NO, SPP
from Lib.Pointer_Entity.Pointer_SGItem import CPointer_SGItem

from Lib.HomeLimit_Entity.HomeLimit_NetObject import CHomeLimit_NO
from Lib.HomeLimit_Entity.HomeLimit_SGItem import CHomeLimit_SGItem

class ESelectionMode( Enum ):
    Select = auto()
    Touch  = auto()

class EAddObjMode( Enum ):
    NoObj        = auto()
    Section      = auto()
    Cell         = auto()
    LaserPointer = auto()

SGItemDesc = namedtuple( "SGItemDesc", "create_func delete_func update_func" )

class CItem_Scene_Manager( QObject ):
    itemTouched = pyqtSignal( QGraphicsItem )

    @property
    def bReadOnly( self ): return self.__bReadOnly

    @bReadOnly.setter
    def bReadOnly( self, value ):
        self.__bReadOnly = value

        for GItem in self.GItems.values():
            if self.__bReadOnly:
                flags = GItem.flags() & ~QGraphicsItem.ItemIsMovable
            else:
                flags = GItem.flags() | QGraphicsItem.ItemIsMovable
            GItem.setFlags( flags )

    def __init__(self, gScene, gView):
        super().__init__()

        self.GItems = {}

        self.bDrawBBox      = False
        self.__bReadOnly      = False

        self.gScene = gScene
        self.gView  = gView

        gView.installEventFilter(self)
        gView.viewport().installEventFilter(self)
        gScene.installEventFilter(self)

        # self.gScene.setMinimumRenderSize( 3 )
        # self.gView.setViewport( QGLWidget( QGLFormat(QGL.SampleBuffers) ) )
        # self.gView.setViewport( QOpenGLWidget( ) )
        # self.gScene.setBspTreeDepth( 1 )
        self.GItems_netObjTypes = { CSection_NO   : CSection_SGItem,
                                    CCell_NO      : CCell_SGItem,
                                    CPointer_NO   : CPointer_SGItem,
                                    CHomeLimit_NO : CHomeLimit_SGItem }

        CNetObj_Manager.addCallback( EV.ObjCreated,       self )
        CNetObj_Manager.addCallback( EV.ObjPrepareDelete, self )
        CNetObj_Manager.addCallback( EV.ObjPropUpdated,   self )

        self.disabledTouchTypes = [ type(None) ]
        self.selectionMode = ESelectionMode.Select
        self.addObjMode    = EAddObjMode.NoObj

        self.objReloadTimer = QTimer()
        self.objReloadTimer.setInterval(500)
        self.objReloadTimer.setSingleShot( True )
        self.objReloadTimer.timeout.connect( self.updateRelationObjects )
        self.objReloadTimer.start()

        self.init()

    def updateRelationObjects( self ):
        for GItem in self.GItems.values():
            GItem.updatePos()

    def init(self):
        self.SceneRoot_ParentGItem = CDummy_GItem()
        self.gScene.addItem( self.SceneRoot_ParentGItem )
        self.bHasChanges = False

    @time_func( sMsg="Scene clear time =" )
    def clear(self):
        self.GItems = {}
        self.gScene.clear()
        self.SceneRoot_ParentGItem = None
        self.gScene.update()
        
    def setDrawBBox( self, bVal ):
        self.bDrawBBox = bVal
        self.gScene.update()

    #############################################################

    def alignGItemsVertical(self):
        GItems = self.gScene.orderedSelection
        for nodeGItem in GItems:
            nodeGItem.netObj()[ SGA.x]  = GItems[0].netObj()[ SGA.x ]

    def alignGItemsHorisontal(self):
        GItems = self.gScene.orderedSelection
        for nodeGItem in GItems:
            nodeGItem.netObj()[ SGA.y ] = GItems[0].netObj()[ SGA.y ]

    #############################################################
    
    clickEvents = [ QEvent.GraphicsSceneMousePress, QEvent.GraphicsSceneMouseRelease, QEvent.GraphicsSceneMouseDoubleClick ]

    AddObj_Parent  = { EAddObjMode.Section : type(None),  EAddObjMode.Cell : CSection_SGItem }
    AddObj_Type    = { EAddObjMode.Section : CSection_NO, EAddObjMode.Cell : CCell_NO }
    def genSectionName( parentNO ):
        nums = [0]
        for child in parentNO.children:
            l = child.name.split( "_" )
            if len(l) > 1:
                try:
                    num = int( l[1] )
                    nums.append( num )
                except ValueError:
                    pass # not integer, just pass
        newNum = max(nums) + 1
        return f"Section_{ newNum }"
    def getCellName( parentNO ):
        nums = [0]
        for child in parentNO.children:
            try:
                num = int( child.name )
                nums.append( num )
            except ValueError:
                pass # not integer, just pass
        newNum = max(nums) + 1
        return f"{ newNum }"
    genAddObj_Name = { EAddObjMode.Section : genSectionName, EAddObjMode.Cell : getCellName }
    
    def eventFilter(self, watched, event):
        if event.type() in CItem_Scene_Manager.clickEvents:
            if self.selectionMode == ESelectionMode.Touch:
                targetGItem = self.gScene.itemAt( event.scenePos() , self.gView.transform() )

                # блокирование снятия выделения с итема, когда активирован режим "Touch" при клике на пустом месте
                # или при клике по элементам сцены для которых не определена возможность "Touch"
                if type( targetGItem ) in self.disabledTouchTypes:
                    event.accept()
                    return True
                                
                if event.type() == QEvent.GraphicsSceneMouseRelease:
                    self.itemTouched.emit( targetGItem )
                    self.selectionMode = ESelectionMode.Select
                
                event.accept()
                return True
    
        #########################################

        if self.__bReadOnly: return False

        #добавление
        if event.type() == QEvent.GraphicsSceneMousePress and event.button() == Qt.LeftButton:
            if self.addObjMode != EAddObjMode.NoObj:
                targetGItem = self.gScene.itemAt( event.scenePos() , self.gView.transform() )

                # проверка на то, что элемент добавляется на разрешенный элемент под ним
                if type(targetGItem) == CItem_Scene_Manager.AddObj_Parent[ self.addObjMode ]:

                    objType = CItem_Scene_Manager.AddObj_Type[ self.addObjMode ]
                    attr = copy.deepcopy( objType.def_props )

                    if self.addObjMode == EAddObjMode.Section:
                        parentNO = sectionsNodeCache()
                        pos = event.scenePos()
                    elif self.addObjMode == EAddObjMode.Cell:
                        parentNO = targetGItem.netObj()
                        pos = targetGItem.mapFromScene( event.scenePos() )

                    name = CItem_Scene_Manager.genAddObj_Name[ self.addObjMode ]( parentNO )

                    attr[ SGA.x ] = round( pos.x() )
                    attr[ SGA.y ] = round( pos.y() )

                    objType( name = name, parent = parentNO, props = attr )

                #     ##TODO: разобраться и починить ув-е размера сцены при добавление элементов на ее краю
                #     # self.gScene.setSceneRect( self.gScene.itemsBoundingRect() )
                #     # self.gView.setSceneRect( self.gView.scene().sceneRect() )

                event.accept()
                return True

        #удаление итемов
        if event.type() == QEvent.KeyPress and event.key() == Qt.Key_Delete:
            for item in self.gScene.selectedItems():
                item.destroy_NetObj()
            event.accept()
            return True
        
        event.ignore()
        return False

    def moveGItems( self, GItems, x = 0, y = 0 ):
        for GItem in GItems:
            GItem.netObj().x += x
            GItem.netObj().y += y

    #############################################################

    @time_func( sMsg="Create scene items time", threshold=10 )
    def ObjCreated(self, netCmd=None):
        netObj = CNetObj_Manager.accessObj( netCmd.Obj_UID )

        if not type( netObj ) in self.GItems_netObjTypes: return
        GItemType = self.GItems_netObjTypes[ type( netObj ) ]

        if self.GItems.get ( netObj.UID ): return

        parentGItem = self.SceneRoot_ParentGItem if type(netObj) in [CPointer_NO, CSection_NO, CHomeLimit_NO] else self.GItems[ netObj.parent.UID ]
        GItem = GItemType ( self, netObj = netObj, parent=parentGItem )
        self.GItems[ netObj.UID ] = GItem
        GItem.init()

        self.bHasChanges = True
        self.objReloadTimer.start()

    def ObjPrepareDelete(self, netCmd):
        netObj = CNetObj_Manager.accessObj( netCmd.Obj_UID )

        if not type( netObj ) in self.GItems_netObjTypes: return

        GItem = self.GItems.get ( netObj.UID )
        if GItem is None: return

        GItem.done()
        netObj.destroyChildren()
        self.gScene.removeItem ( GItem )
        del self.GItems[ netObj.UID ]
        del GItem

        self.bHasChanges = True
        self.objReloadTimer.start()

    def ObjPropUpdated(self, netCmd):
        netObj = CNetObj_Manager.accessObj( netCmd.Obj_UID )

        if not type( netObj ) in self.GItems_netObjTypes: return

        GItem = self.GItems[ netObj.UID ]
        if netCmd.sPropName in SGA.xyProps:
            GItem.init()
        elif netCmd.sPropName in SGA.whProps:
            GItem.init()
            GItem.update()
        elif netCmd.sPropName in SPP.taskProps:
            GItem.updatePos()
            GItem.update()

        self.bHasChanges = True
        self.objReloadTimer.start()

    #############################################################
    def selectItemsByUID( self, objSet ):
        s = set()

        oldSelItems = set( self.gScene.selectedItems() )
        selItems = set()

        for item in self.GItems.values():
            if not objSet.isdisjoint( item.getNetObj_UIDs() ):
                selItems.add( item )
                item.setSelected( True )

        deSelectItems = oldSelItems - selItems

        for item in deSelectItems:
            item.setSelected( False )
