import weakref

from PyQt5.QtWidgets import QGraphicsItem
from PyQt5.QtCore import QRectF

class CBase_GItem(QGraphicsItem):
    fBBoxD  =  20 # расширение BBox для удобства выделения

    def __init__(self, ISM, netObj, parent ):
        super().__init__( parent=parent )
        self.ISM = ISM
        self.netObj = weakref.ref( netObj )
        self.setFlags( QGraphicsItem.ItemIsSelectable | QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemClipsChildrenToShape )

    def calcBBox(self):
        self.prepareGeometryChange()
        self.BBoxRect = QRectF( 0, 0, self.netObj().w, self.netObj().h )
        d = self.fBBoxD
        self.BBoxRect_Adj = self.BBoxRect.adjusted(-1*d, -1*d, d, d)

    def destroy_NetObj( self ):
        if self.netObj(): self.netObj().destroy()

    def getNetObj_UIDs( self ): return { self.netObj().UID }

    def boundingRect(self): return self.BBoxRect_Adj

    def init(self):
        self.calcBBox()
        self.updatePos()
        self.setFlag( QGraphicsItem.ItemIsMovable, not self.ISM.bReadOnly )

    def done(self):
        pass

    def setPos(self, x, y):
        self.netObj().x = round(x)
        self.netObj().y = round(y)

        super().setPos( self.netObj().x, self.netObj().y )
    
    def updatePos(self):
        self.setPos( self.netObj().x, self.netObj().y )
    
    def move(self, deltaPos):
        pos = self.pos() + deltaPos
        self.setPos(pos.x(), pos.y())

    def mousePressEvent(self, event):
        self.startMovePos = event.pos()
        super().mousePressEvent( event )

    def mouseMoveEvent( self, event ):
        selfItemType = type( self )
        pos = self.mapToScene ( event.pos()-self.startMovePos )

        #привязка к сетке
        if self.scene().bSnapToGrid:
            gridSize = self.scene().gridSize
            def snap( a ):
                snap_a = round( a/gridSize ) * gridSize
                if abs(a - snap_a) < gridSize/5: a = snap_a
                return a
            pos.setX( snap( pos.x() ) )
            pos.setY( snap( pos.y() ) )

        pos = self.mapFromScene( pos )

        for gItem in self.scene().selectedItems():
            if not isinstance(gItem, selfItemType): continue

            if not bool(gItem.flags() & QGraphicsItem.ItemIsMovable): return
            

            gItem.move( pos )
