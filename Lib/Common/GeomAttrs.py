from Lib.Common.StrProps_Meta import СStrProps_Meta

class SGeomAttrs( metaclass = СStrProps_Meta ):
    x = None
    y = None
    z = None
    w = None
    h = None

SGA = SGeomAttrs
SGA.xyProps   = [ SGeomAttrs.x, SGeomAttrs.y ] # type:ignore
SGA.whProps   = [ SGeomAttrs.w, SGeomAttrs.h ] # type:ignore
SGA.baseProps = [ SGeomAttrs.x, SGeomAttrs.y, SGeomAttrs.w, SGeomAttrs.h ] # type:ignore
