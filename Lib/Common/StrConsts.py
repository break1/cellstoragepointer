
from Lib.Common.StrProps_Meta import СStrProps_Meta

class SC( metaclass = СStrProps_Meta ):
    last_opened_file    = None
    main_window         = None
    geometry            = None
    state               = None
    propRef             = None
    name                = None
    UID                 = None
    some_node           = None
    localhost           = None
    sWarning            = "[Warning]:"
    sError              = "[Error]:"
    sAssert             = "[Assert]:"
    Pointers            = None
    Sections            = None
    Limits              = None
    defSceneFName       = "Undefined.json"

def genSplitPattern( *DS_List ):
    l = []
    for DS in DS_List:
        pattern = f" {DS} | {DS}|{DS} |{DS}"
        l.append( pattern )

    return "|".join( l )

