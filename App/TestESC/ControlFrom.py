from PyQt5 import uic

from PyQt5.QtCore import QTimer, pyqtSlot
from PyQt5.QtWidgets import QWidget

import Lib.Common.FileUtils as FU
from Lib.esclib.pyESC import CEscDev

class CControlForm(QWidget):
    def __init__(self):
        super().__init__()
        uic.loadUi( FU.UI_fileName( __file__ ), self )
        self.pDev = None

        self.mainTimer = QTimer()
        self.mainTimer.setInterval( 500 )
        self.mainTimer.timeout.connect( self.onTick )
        self.mainTimer.start()

        self.bRotateCycleA = False
        self.bRotateCycleB = False

    def on_btnConnect_released( self ):
        self.pDev = CEscDev( self.leIPAddress.text(), self.sbPortNum.value() )

    def on_btnDisconnect_released( self ):
        self.pDev = None

    def on_btnRotate_released( self ):
        self.pDev.rotate( self.sbAngleA.value(), self.sbAngleB.value() )

    def on_btnGetStatus_released( self ):
        self.lbStatus_val.setText( str( self.pDev.status() ) )

    @pyqtSlot(bool)
    def on_btnRotateCycleA_clicked( self, bVal ):
        self.bRotateCycleA = bVal

    @pyqtSlot(bool)
    def on_btnRotateCycleB_clicked( self, bVal ):
        self.bRotateCycleB = bVal

    def onTick( self ):
        bConnected = not self.pDev is None
        self.btnConnect.setEnabled( not bConnected )
        self.btnRotateCycleA.setEnabled( bConnected )
        self.btnRotateCycleB.setEnabled( bConnected )
        self.btnRotate.setEnabled( bConnected and ( not (self.bRotateCycleA or self.bRotateCycleB ) ) )
        self.btnGetStatus.setEnabled( bConnected )

        if bConnected and ( self.bRotateCycleA or self.bRotateCycleB ):
            a = self.sbAngleA.value() if self.bRotateCycleA else 0
            b = self.sbAngleB.value() if self.bRotateCycleB else 0
            self.pDev.rotate( a, b )


