import sys

from PyQt5.QtWidgets import QApplication, QWidget
from .ControlFrom import CControlForm

def main():
    app = QApplication( sys.argv )

    w = CControlForm()
    w.show()

    app.exec_()
