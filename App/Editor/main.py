import sys

from Lib.Common.BaseApplication import baseAppRun
from Lib.SceneViewer.ViewerWindow import CViewerWindow, EWorkMode
import Lib.AppCommon.NetObj_Registration as NOR

def main():
    mainWindowParams = {
                            "windowTitle" : "Scene Editor : ",
                            "workMode" : EWorkMode.DesignerMode
                        }
    return baseAppRun( bNetworkMode = False,
                       mainWindowClass = CViewerWindow, mainWindowParams=mainWindowParams,
                       register_NO_Func = ( NOR.register_NetObj, NOR.register_NetObj_Props ),
                       rootObjDict = NOR.rootObjDict )
